% Dokumentenklasse fuer Artikel waehlen
\documentclass[
% final,
paper=a4,fontsize=12pt,twocolumn=false,titlepage]{scrartcl}

 \usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2cm,footskip=1cm]{geometry}

% Zur Einbindung von Grafiken mit \includegraphics
\usepackage[pdftex]{graphicx,color}
\DeclareGraphicsExtensions{.pdf,.jpg,.png,.eps,.ps}
\usepackage{graphicx}

\usepackage[english,ngerman]{babel}
\usepackage[style=verbose-ibid,isbn=false,doi=false,backend=biber]{biblatex}
\addbibresource{references.bib}
\AtEveryCitekey{
% \clearfield{isbn}% } \AtEveryBibitem{% \clearfield{isbn}%
}

% Korrekte Umsetzung von Umlauten
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% Make title makros available
\usepackage{titling}

% Times fonts
\usepackage{txfonts}

% Todo
\usepackage[textsize=small,obeyFinal]{todonotes}
\presetkeys{todonotes}{fancyline}{}

\usepackage{hyperref}

\usepackage[autostyle]{csquotes}

\usepackage{verbatim}

\usepackage[stable]{footmisc}

% Micro-typographic enhancements
\usepackage{microtype}

% Einrueckung der ersten Zeile eines Absatzes
\setlength{\parindent}{0em}

% Abstand zwischen Absaetzen
\setlength{\parskip}{1.5ex plus0.5ex minus0.5ex}

% Seitenstil
\usepackage{scrpage2}
\pagestyle{useheadings}

% Silbentrennungsliste
\hyphenation{}

\usepackage{setspace}
\spacing{1.5}
\begin{document}

\author{Simon-Martin Schröder}
\title{Fahrerlose Fahrzeuge}

\makeatletter \hypersetup{hidelinks,linktoc=all,pdfauthor={\@author},
pdftitle={\@title}, pdfsubject={\@subject}} \makeatother

\hypersetup{pageanchor=false}
\begin{titlepage}
\pagenumbering{gobble}
\centering
Christian-Albrechts-Universität -- Philosophisches Seminar\\[3cm]

\textbf{- Essay -}\\[1cm]

{\large \thetitle}

\vfill

\raggedright
Risikoethik\\
Moritz Riemann, M.A.

Vorgelegt von: 

Simon-Martin Schröder\\
Mat.-Nr. 5279\\

Weißenburgstr. 45\\
24116 Kiel\\
Tel: 0151 18901404\\
E-Mail: martin.schroeder@nerdluecht.de
\end{titlepage}
\hypersetup{pageanchor=true}

% Seitennummerierung
\pagenumbering{arabic}

\begin{quote}
Eine Minute, bevor ein glücklicher Mann ein Kind tötet, ist er noch glücklich, und eine Minute, bevor eine Frau vor Entsetzen
aufschreit, kann sie noch mit geschlossenen Augen vom Meer träumen, und während der letzten Minute im Leben eines Kindes können
die Eltern dieses Kindes in einer Küche sitzen und auf den Zucker warten und von den weißen Zähnen ihres Kindes sprechen und von
einer Bootsfahrt, und das Kind selbst kann eine Pforte schließen und sich anschicken, mit ein paar Stückchen Zucker in einem
weißen Papier in seiner rechten Hand eine Landstraße zu überqueren, und es kann diese ganze letzte Minute lang nichts anderes vor
Augen sehen als einen langen, blanken Fluß mit großen Fischen darin und einen breiten Kahn mit stillen Rudern.

Nachher ist alles zu spät.\footcite{dagermann48}
\end{quote}

In der Kurzgeschichte \emph{Ein Kind töten} von Stig Dagerman geht es um einen
Autounfall, bei dem ein Kind tödlich verunglückt, weil beide -- das Kind und der
Fahrer -- unaufmerksam sind.
Wer sich einmal selbst beim Autofahren beobachtet hat, dem sind sicher sicher
zahlreiche Situationen aufgefallen, in denen nur nichts passiert, weil Murphy's
Law doch nicht so unbarmherzig ist, wie immer behauptet wird. Der Mensch wird
von vielen Faktoren beeinflusst und das Lenken des Fahrzeugs ist fast nie die
einzige Aufgabe, die er auf dem Fahrersitz erfüllt. Wäre es nicht besser, wenn
ein Fahrer immer aufmerksam und nie müde wäre, sich nur auf das Fahren
konzentrieren würde und immer die komplette Umgebung des Fahrzeugs im Blick
hätte?

In diesem Essay wird die Ansicht vertreten, dass es aus risikoethischer Sicht
geboten ist, von Menschen gelenkte Fahrzeuge durch fahrerlose abzulösen. Dies
wird mit der Unzulänglichkeit menschlicher Fahrer sowie den neuartigen Möglichkeiten
hinsichtlich Mobilität und Ressourcenschonung begründet.
Eine vollständige Verbannung menschlicher Fahrer aus dem Straßenverkehr ist die
höchste Stufe des automatisierten Fahrens. Reisende wären dann alleinig
Fahrgäste und von ihnen würden abseits der Zielvorgabe keine Eingriffe in die
Steuerung des Fahrzeugs erwartet. Ein Computer übernähme unbeschränkt die
Kontrolle des Fahrzeugs (Steuern, Bremsen, Beschleunigen, Überwachen von
Fahrzeug und Straße), die Planung von Manövern (bspw. Spurwechsel) und reagierte
selbständig auf Ereignisse.\footcite{sae:automated_driving}

Im Folgenden werden zunächst einige mögliche Schwächen fahrerloser Fahrzeuge
untersucht und gegebenenfalls entkräftet. Anschließend wird die Bedeutung
fahrerloser Fahrzeuge für Menschen und Umwelt betrachtet sowie für ihre
Überlegenheit gegenüber menschlicher Fahrer argumentiert. Zum Schluss
werden Vor- und Nachteile autonomer Fahrzeuge anhand des Vorsorgeprinzips
bewertet und eine Prognose gewagt.

\vspace{1cm}

%%
% Antithese
Zunächst wird man die Forderung nach einem fahrerlosen Verkehr wahrscheinlich
für reinen Technikenthusiasmus halten.
Nur weil etwas technisch möglich ist, muss es nicht wünschenswert sein. Im
weiteren Verlauf wird allerdings deutlich werden, dass dieser Vorwurf nicht
haltbar ist und die Forderung von starken Argumenten unterstützt wird.

Ein entscheidendes Argument gegen die fortschreitende Verschiebung der
Verantwortung für das Wohlergehen von Verkehrsteilnehmern auf technische Systeme
ist die Möglichkeit des technischen Versagens. Allerdings beziehen sich heutige
Fälle von technischem Versagen üblicherweise auf Systeme, die nach festen Regeln
Entscheidungen von Menschen umsetzen.
Das Ergebnis einer Fehlfunktion in diesen Systemen bleibt davon unberührt, wer
(oder was) die zielbestimmende Entscheidung gefällt hat.
Relevant sind an dieser Stelle hingegen Fehler des Entscheidenden.
Im herkömmlichen Sinne ist dies ein Mensch, im Fall von intelligenten Maschinen
wie autonomen Fahrzeugen ist es aber die Maschine selbst.

Es lässt sich argumentieren, dass Maschinen auch angesichts der
Möglichkeit einer Fehlfunktion die besseren Entscheider sind.

Der heutzutage übliche Grad der Automatisierung ermöglicht es, dass einzelne
Personen Prozesse steuern, die das Leben vieler anderer Menschen berühren.
Geringfügige Bedienfehler können so zu großem Leid führen. Dies war etwa der
Fall beim Zugunglück von Bad Aibling, das sich erst dadurch ereignen konnte,
dass der Fahrdienstleiter zunächst eine eingleisige Strecke für zwei Züge
freigab und seine anschließende Warnung die Lokführer nicht erreichte, weil er
die falsche Notruftaste drückte.\footcite{spiegel:falschen-knopf} Die hohe
Automatisierung und die Abstraktion großer Zusammenhänge hinter einfachen
Bedienelementen hat hat hier dazu geführt, dass einzelne Personen die
Verantwortung für weitreichende Entscheidungen tragen. Die Übertragung der
Entscheidungsgewalt von Personen auf Maschinen würde die Zahl der affizierten
Personen nicht beeinflussen.

Technisches Versagen ist ohnehin schon seltener als menschliches Versagen. So
macht PlaneCrashInfo.com\footcite{planecrashinfo:cause}, eine Datenbank über
Flugunfälle, Pilotenfehler für 60\% und mechanische Fehler für 18\% der
Unfälle verantwortlich\footnote{Dabei beinhaltet die Kategorie
\enquote{Mechanische Fehler} auch Design- und Wartungsfehler, die auf
menschliche Fehler zurückzuführen sind.}.
Während fahrerlose Fahrzeuge mechanisch nahezu identisch mit konventionellen
Fahrzeugen sind und mechanische Fehler oder Manipulationen also nicht häufiger
auftreten sollten, kommt die Möglichkeit des Versagens oder der Manipulation der
zentralen Steuerkomponente hinzu. Es gilt, diese Risiko gegen das Risiko von menschlichen
Fehlentscheidungen abzuwägen.

Die korrekte Funktion aller Systemkomponenten eines selbstfahrenden Fahrzeugs ab
Werk ließe sich am besten dadurch feststellen, dass sie durch eine maximale
Anzahl von Experten überprüft werden. Hersteller sollten ihnen also Einblick in
den Aufbau kritischer Soft- und Hardware geben. Im Idealfall sollten diese
Details tatsächlich komplett öffentlich sein, sodass eine möglichst große Zahl
von Personen eventuelle Fehler finden kann. Zusätzlich sollten automatische
Testverfahren zum Einsatz kommen. Entsprechend begutachtete Komponenten könnten
zertifiziert werden. Unzertifizierte Fahrzeuge ließen sich dann vom öffentlichen
Straßenverkehr ausschließen.

Desweiteren lässt sich jedes technische System manipulieren. Je komplexer es
ist, desto weniger offensichtlich ist eine Manipulation. Dabei
betreffen Veränderungen im ausführenden Teil eines technischen Systems oder die
Fälschung von Daten, auf deren Basis Entscheidungen gefällt werden,
sowohl von Menschen gelenkte als auch fahrerlose Fahrzeuge.

Die Manipulation der Steuerkomponente hingegen ist äquivalent zur Beeinflussung
eines menschlichen Entscheiders. Ist ein Angreifer physisch anwesend, kann er in
beiden Fällen sehr einfach Einfluss nehmen. Durch Sicherungseinrichtungen kann
eine solche Situation häufig bemerkt und auch verhindert werden. Doch auch aus
der Ferne kann er menschliche und maschinelle Entscheider beeinflussen (etwa durch
Ausnutzung von Sicherheitslücken bzw. Erpressung oder Bestechung).
Letztendlich ist absolute Sicherheit in beiden Fällen unmöglich. Ein modulares,
dezentrales und redundantes System, dessen Parameter und Funktion in
angemessener Weise durch technische Überwachungssysteme und menschliche
Beobachter überwacht werden, hat jedoch eine größere Chance einer böswilligen
Manipulation zu widerstehen als ein einzelner Mensch.

Um ihre zuverlässige Funktionalität zu erhalten, dürften Reparaturen an
autonomen Fahrzeugen nur professionell durchgeführt werden. Vor allem sensorische und
entscheidende Komponenten müssten vor fremdem Zugriff geschützt werden. Das mag
zwar bedeuten, dass ein Fahrzeughalter seinen Wagen nicht mehr selbst reparieren
kann, doch dies ist auch schon bei gegenwärtigen Fahrzeugen die Regel und durch
gesetzliche Verbote reglementiert.

Manipulationen an mechanischen und elektronischen Komponenten könnten durch ein
intelligentes Fahrzeug selbst erfasst werden und es wäre in der Lage, darauf zu
reagieren, indem es seine Funktion versagt und den Vorfall an zuständige Stellen
weiterleitet.
Vernetzte Fahrzeuge hätten die Möglichkeit, gegenseitig ihre Integrität
überprüfen und wenn nötig auffällige Verkehrsteilnehmer ausschließen. Hierbei
könnte auch die oben erwähnte Zertifizierung von Komponenten eine Rolle spielen.

Die Vernetzung untereinander ist ein elementarer Bestandteil der Vorteile
selbstfahrender Fahrzeuge (s.u.). Dennoch stellt diese Vernetzung
verständlicherweise auch ein größeres Risiko dar. Während
schon ein einzelnes manipuliertes Fahrzeug einen großen Schaden anrichten kann,
kann die Wirkung einer koordinierten Manipulation vieler Fahrzeuge oder gar
eines zentralen Verkehrssteuerungssystems verheerend sein. Daher sollte die
Steuerung des Verkehrs möglichst dezentral erfolgen und trotz einer Vernetzung
muss jedes Fahrzeug möglichst autonom entscheiden. Kommunikationsschnittstellen
von Fahrzeugen, heutigen und zukünftigen, müssen besonders geschützt werden. Es
dürfen nur genau definierte Daten übertragen werden und jedes Fahrzeug muss alle
zur Verfügung stehenden Informationen mit dem nötigen Misstrauen betrachten und
anhand eigener Informationen verifizieren.
Auf diese Weise kann das Risiko einer großangelegten Manipulation minimiert
werden.

Entgegen der verbreiteten Furcht vor Fehlfunktion oder Manipulation wurden
bisher sehr gute Erfahrungen mit fahrerlosen Fahrzeugen gemacht.
Die Metro Kopenhagen setzt selbstfahrende Züge ein, die nur in
Ausnahmesituationen von den anwesenden \enquote{Metro Stewards} gesteuert
werden.\footcite{Jensen2002} Denmarks Statistik, das dänische Äquivalent zum
Statistischen Bundesamt, verzeichnet seit ihrer Eröffnung am 19.~Oktober 2002
keinen einzigen Unfall, bei dem Menschen verletzt oder gar getötet wurden. Dem
gegenüber stehen sieben getötete und 20 verletzte Opfer des landesweiten
konventionellen Schienennetzes in den Jahren 2002 bis 2003 und ähnliche Zahlen
in den nachfolgenden Jahren.\footcite{dst.dk:traffic-accidents} Googles
\emph{Self-Driving Car} erlebte erst in diesem Jahr den ersten Unfall, der nicht
von Menschen verschuldet war. Zuvor war es bei einer Fahrleistung von ca. 2 Mio.
Kilometern in 17 fremdverschuldete Unfälle mit insgesamt einer leicht verletzten
Person verwickelt.\footcite{wired:first-crash} Im normalen Straßenverkehr wurden
im Jahr 2013 auf US-amerikanischen Straßen auf die gleiche Fahrleistung
gerechnet ca. 1000 Menschen verletzt und 14 getötet.\footcite{nhtsa2014} Wie
zuvor angedeutet, geht also tatsächlich schon von heutigen fahrerlosen
Fahrzeugen eine geringere Unfallgefahr aus als von solchen, die von Menschen
gelenkt werden.

Für viele Menschen stellt ein Auto weit mehr da als ein einfaches
Fortbewegungsmittel. Eine volle Automatisierung des Fahrens dürfte bei
Liebhabern einer sportlichen Fahrweise zunächst auf Ablehnung stoßen.
Allerdings gibt es keinen vernünftigen Grund, einzelnen eine riskante Fahrweise
auf Kosten der Sicherheit Dritter zuzugestehen. Zudem wird ein optimal
entscheidendes Auto sogar in der Lage sein, die sportliche Fahrweise eines
menschlichen Fahrers zu übertreffen, während die Unfallwahrscheinlichkeit
minimiert wird.

\begin{comment}
- OK Reiner Technikenthusiasmus
- Risiko technisches Versagen. -> Technisches Versagen auf menschliches Versagen zurückzuführen. Viele-Augen-Prinzip schützt.
- Risiko Manipulation \url{https://www.ted.com/talks/avi_rubin_all_your_devices_can_be_hacked}
	- Individuelles Fahrzeug
		- Begrenzter Schaden (check)
	- Infrastrukturlenkung
		- Immenser Schaden möglich -> Vermeiden durch Dezentralität (Steuerung im einzelnen Fahrzeug) (Check)
	- "Optimierungen" durch Benutzer -> Kryptographische Signierung von Komponenten (check)
- Kein Fahrspaß -> Situationsangepasst im Rahmen der Sicherheit maximal
sportliche Fahrweise (check)
- Nicht selbst reparieren (Check)
\end{comment}

\begin{comment}
All your devices can be hacked
- Implanted medical devices: ICD (defibrillator)
    - Turn off
    - Trigger fibrillation
- Automobiles
    - internal wired network
        - change speedometer value
        - apply brakes
        - disable brakes
        - install malware
    - wireless network
         - short range: wheel sensor
         - long range: digital radio
    - activate malware via wireless network

"be aware that devices can be compromised, and anything that has software in it is going to be vulnerable. It's going to have bugs."
\end{comment}

\vspace{1cm}

%%
% \section{[These]}

Nachdem nun mögliche Schwächen fahrerloser Fahrzeuge betrachtet wurden, gilt der
folgende Abschnitt ihren Stärken.

Ein Verkehr ohne Fahrer bedeutet eine radikale Veränderung aller Aspekte, die
ihn betreffen. Durch fahrerlose Fahrzeuge kann gleichzeitig sowohl der Zugang zu
individueller Mobilität verbessert und damit die Anzahl der Nutznießer des
Verkehrs erhöht als auch das Verkehrsaufkommen und die damit verbundenen Kosten
für Unbeteiligte reduziert werden.

Der Mensch in der modernen Gesellschaft hat eine Vielzahl von Bedürfnissen, die
nicht selten außerhalb der per pedes erreichbaren Grenzen liegen. Um erfolgreich
an dieser Gesellschaft teilnehmen zu können, muss der Mensch also \emph{mobil}
sein. Dabei ist \emph{Verkehr} das Instrument, das Mobilität
ermöglicht.\footcite[S.
334f.]{becker2013} Erklärtes Ziel der deutschen Verkehrspolitik ist die
Maximierung der Verkehrsmenge. Eine leistungsfähige Infrastruktur sei die
entscheidende Voraussetzung für Mobilität.\footcite{bmvi:verkehr-mobilitaet}
Diese Zielsetzung ist allerdings kritikwürdig, denn während der Nutzen des
Verkehrs in der Regel allein den Reisenden zugute kommt, verursacht er eine
Reihe von Kosten, die meist externalisiert, also durch die von der gegenwärtigen
und zukünftigen Gesellschaft getragen werden.\footcite[S. 333]{becker2013} Dazu
gehören unter anderem Lärm, Abgase, Unfallkosten, Klimakosten, Trennwirkung und
Abfälle.
Daher ist es stattdessen sinnvoll, die \emph{individuelle Mobilität} zu
optimieren und dabei das Verkehrsaufkommen zu minimieren.\footcite[S.
334f.]{becker2013} Der große Vorteil fahrerloser Fahrzeuge ist nun, dass sie auf
mehrfache Weise das Bedürfnis nach gesteigerter Mobilität befriedigen können,
und das bei reduziertem Verkehr und damit reduzierten Kosten für die
Allgemeinheit.

Durch eine optimale algorithmische Steuerung können fahrerlose Fahrzeuge ihre
Fahrweise so anpassen, dass Materialverschleiß und Energieaufwand minimiert
werden. Dadurch würden sie eine höhere Laufleistung erreichen und weniger
Schadstoffe ausstoßen, wodurch weniger Folgekosten durch die Produktion und
Kraftstoffherstellung und -verbrennung entstehen. Auf diese Weise können Sie
einen Beitrag zum Schutz der Umwelt leisten, wodurch die Welt auch für
zukünftige Generationen von Menschen bewohnbar bleibt.
Elektroantriebe erlauben darüber hinaus einen weitaus geringeren
Schadstoffausstoß als konventionelle Verbrennungsmotoren, wobei natürlich der
Strom-Mix mitverantwortlich ist für die Höhe der Emissionen. Derzeit wird
der Einsatz von elektrischen Antrieben jedoch durch die begrenzte
Reichweite der fahrzeugeigenen Stromspeicher und die geringe Verfügbarkeit von
Aufladestationen limitiert. Selbstfahrende Fahrzeuge könnten diese
Beschränkungen überwinden, da sie auch ohne einen Fahrer bei Nichtbenutzung eine Aufladestation
anfahren können und so die meiste Zeit ein vollständig geladenes Fahrzeug zur
Verfügung steht.

\begin{comment}
- Einsparung von Material, Energie und Emissionen -> Risiko Resourcenverschwendung. Welt bleibt bewohnbar
	- sparsames (Verbrauch) & abnutzungsarmes (Verbrauch) Fahren dank Optimierung des Fahrverhaltens
		- optimierte Geschwindigkeit mit langer Vorausplanung
		- Lenkbewegung optimiert hinsichtlich Verschleiß
	- Förderung von Elektromobilität
		- Automatisches Aufladen, zeit- und ortsunabhängig vom Benutzer (z.B. nachts an öffentlicher Ladestation)
\end{comment}

\begin{comment}
- Gesellschaftlicher Nutzen
	- Zeitersparnis (man kann die Fahrzeit produktiv nutzen)
	- Günstigere Mobilität im Falle von ÖPNV
\end{comment}

Neben der Allgemeinheit belastet der derzeitige Straßenverkehr auch die Fahrer.
Es erscheint wenig sinnvoll, dass ein Fahrer mehrere Stunden lang seine gesamte
Aufmerksamkeit und Leistungsfähigkeit allein dem Ortswechsel widmet, sobald es
eine Alternative gibt, in der ihm seine Aufmerksamkeit frei zur Verfügung steht,
während die Reise mindestens ebenso komfortabel bleibt.
Er würde nicht nur den Stress und die damit verbundenen Auswirkungen auf seine Gesundheit sparen,
sondern könnte seine Zeit und Energie frei wählbar einsetzen,
beispielsweise produktiv oder zur Erholung.

\begin{comment}
- Roboter-ÖPNV potenzieren Nutzen
    - Weniger Fahrzeuge insgesamt nötig
	    - Weniger Emissionen
	    - Weniger Verkehrsflächen (mehr Platz für Erholung, Wohnen, ...)
	    - Geringere Verkehrsdichte -> Geringeres Gefahrenpotential für Fußgänger und Radfahrer
    - Günstiger, flexibler, individueller Personennahverkehr
\end{comment}

Ein weiterer Aspekt ist, dass sich die Kosten vervielfachen, wenn für ähnliche
oder sogar gleiche Strecken mehrere Fahrer mehrere Fahrzeuge nutzen. Es werden
sowohl das Verkehrsaufkommen und damit die Verkehrskosten überflüssig erhöht,
als auch die Kosten für das Individuum, da es ein Fahrzeug anschaffen, erhalten
und bedienen muss. Würden gleiche Wege im selben Fahrzeug geteilt (wie es zum
Beispiel in der Bahn der Fall ist), so würden (monetäre und allgemeine)
Mehrkosten verhindert werden.
Der Vorteil von fahrerlosen Autos ist hierbei, dass sogar individuelle Wege von
Tür zu Tür übernommen werden können und wartende Massen an Bahnsteigen
verhindert werden.
Auf längeren gemeinsamen Strecken könnten sich Einzelfahrzeuge sogar
aneinander kuppeln. Dadurch ließe sich Platz auf der Straße und Energie sparen.
Aber auch schon durch den flächendeckenden Einsatz von einzelnen selbstfahrenden
Autos ließe sich die Kapazität vorhandener Straßen um 40\% bis 80\% erhöhen und Staus
weitestgehend vermeiden.\footcite{Pinjari2013} In jedem Fall kämen mehr
Reisende schneller an ihr Ziel.
Selbstfahrende Autos, die sowohl Lebenszeit als auch menschliche Energie und
physikalische Ressourcen sparen und mehrere Personen gleichzeitig befördern,
stellen also die bessere Alternative zu konventionellen Fahrzeugen dar, die
häufig nur ihren eigenen Fahrer transportieren.
Sie ermöglichen eine neue Form des Carsharings oder
öffentlichen Nahverkehrs, bei dem ein Reisender nur seinen Bedarf melden muss
und an Ort und Stelle vom Fahrzeug abgeholt werden kann, anstatt zunächst zum
Fahrzeug gelangen zu müssen. Im Vergleich zum herkömmlichen öffentlichen
Nahverkehr wäre eine individuelle Mobilität auch Personen ohne eigenes Fahrzeug
zugänglich und die Vorteile von günstigen Massenverkehrsmitteln und eigenen
Fahrzeugen ließen sich kombinieren. Schon jetzt bietet der Fahrdienstleister
Uber experimentelle Fahrten mit autonomen Fahrzeugen
an.\footcite{wired:ubers-self-driving-car}
Ein einzelnes gemeinsam genutztes selbstfahrendes Fahrzeug kann so bis zu zwölf
privat genutzte konventionelle Fahrzeuge ersetzen.\footcite{Fagnant2014} Eine
auf diese Weise verminderte Fahrzeugzahl würde die Verkehrskosten weiter
reduzieren.
Da kein Fahrer benötigt wird, ermöglichen selbstfahrende Fahrzeuge auch eine
höhere Mobilität für diejenigen, die kein Fahrzeug steuern können oder dürfen.
Dies betrifft beispielsweise Kinder, alte Menschen und Menschen mit Behinderung.

\begin{comment}
- Unpredictable behavior of drivers -> car communication
- Sharing position data
- Willingness to share data:
	 - "disconcerting notion, this idea that our cars will be watching us, talking
	 about us to other cars"
	 - Protect privacy
\end{comment}

Ein weiterer Vorteil ergibt sich aus der Vernetzung eigenständiger Fahrzeuge.
Auf diese Weise könnte auf externe Verkehrssteuerung (beispielsweise durch
Ampeln und Schilder) und strikte Aufteilung der Straßen in einzelne Fahrspuren
weitestgehend verzichtet werden.
Die Kommunikation der Fahrzeuge untereinander würde es ihnen erlauben, ihre
Fahrweisen und Manöver abzustimmen, was zu einem dynamischeren Verkehr führen
würde, wodurch Raum auf der Straße eingespart und Warte- und Fahrzeit minimiert
werden könnte.  Dadurch wiederum sänken die Umweltkosten. Aufgrund der
effizienteren Nutzung von Verkehrsflächen könnten diese gerade in dicht
besiedelten Gebieten teilweise in dringend benötigte Wohn- Arbeits- oder
Erholungsflächen umgewandelt werden. Gleichzeitig könnte Fußgängern und
Radfahrern mehr Platz eingeräumt werden, wodurch ihre Sicherheit erhöht würde.

Kritisch muss allerdings die massenhafte Preisgabe von Positions- und
anderen Daten gesehen werden. Zum Schutz der Privatsphäre der Reisenden dürften
nur tatsächlich erforderliche Daten ausgetauscht werden und diese sollten
geeignet anonymisiert werden.

Technik, um Unfälle wie den im einleitenden Zitat beschriebenen zu verhindern,
gibt es schon heute.\footnote{Bspw.
\url{https://www.youtube.com/watch?v=u80eraX8IJQ}} Allerdings sind heutzutage
regulär käufliche Fahrzeuge noch immer auf die Kontrolle durch einen Fahrer
angewiesen. Doch autonome Versuchsfahrzeuge beweisen bereits ihre Überlegenheit
gegenüber von Menschen gelenkten Fahrzeugen.

\begin{comment}
https://www.wired.com/2016/02/googles-self-driving-car-may-caused-first-crash/
24.10.16
Google’s cars have driven more than
1.3 million miles since 2009. They can recognize hand signals from traffic
officers and “think” at speeds no human can match. As of January, they had been
involved in 17 crashes, all caused by human error.
\end{comment}

\begin{comment}
- Menschliches Versagen wird minimiert -> Risikofaktor Mensch
    - Übermüdete Fahrer (nach langer Arbeit, anstrengendes Wochenende)
    - Ablenkung (Telefonieren, Streit, Familienchaos, Aufmerksamkeitserregendes am Straßenrand, Emotionale Erlebnisse zuvor)
    - Emotionen im Straßenverkehr (Ärger über Verkehrsteilnehmer, Stau)
    - Drogen- \& Alkoholkonsum
\end{comment}

\begin{comment}
https://www.ted.com/talks/jennifer_healey_if_cars_could_talk_accidents_might_be_avoidable
- Driving is dangerous
- Human body not designed for task
	- Single focus of attention
	- Limited sensing range (motor cyclist was seen by many others before passing)
\end{comment}


Im Zentrum meiner Argumentation steht der Mensch als unperfekter Fahrer.
Zunächst besteht nur eine Minderheit der Verkehrsteilnehmer aus speziell
ausgebildeten, berufsmäßigen Fahrern, die sich wiederholten Schulungen und Tests
unterziehen, während die Mehrheit aus mehr oder weniger erfahrenen Laien
besteht, die einmalig die Führerscheinprüfung absolviert haben.

Darüber hinaus besitzen Menschen zahlreiche Eigenschaften, die sie
eigentlich als Fahrer eines Kraftfahrzeugs disqualifizieren:

Menschen werden müde. Doch nach einem langen Arbeitstag oder einem
anstrengenden Wochenende ist trotzdem noch die Rückreise zu absolvieren. Auch
das Fahren selbst lässt den Fahrer ermüden. Müde Fahrer können nur verzögert auf
unvorhergesehene Ereignisse reagieren. Im Extremfall schlafen sie am Steuer
sogar ein.

Menschen lassen sich leicht ablenken. Die Aufmerksamkeit des Fahrers liegt nur
in seltenen Fällen vollständig auf der Verkehrssituation. Nebenbei wird
telefoniert oder mit Mitreisenden gesprochen, teilweise sogar gestritten, Eltern
sind häufig von ihren Kindern in Anspruch genommen und auch am Straßenrand
können Personen oder Ereignisse die Aufmerksamkeit des Fahrers binden.

Menschen sind emotional. Streit vor oder während der Fahrt, Ärger über
Verkehrsteilnehmer oder -situationen oder auch positive Emotionen wie in der
einleitenden Kurzgeschichte können den Fahrer zu einer unangemessenen Fahrweise
verleiten.

Menschen konsumieren legale und illegale Genussmittel, die ihre Wahrnehmung und
Urteilskraft beeinflussen. Auch benötigen sie manchmal Medikamente mit einer
ähnlichen Wirkung. Obwohl gesetzlich verboten, kommt es vor, dass sie unter diesem
Einfluss am Straßenverkehr teilnehmen.

Darüber hinaus sind Menschen, auch ohne dass sie durch die obigen Faktoren
beeinträchtigt wären, in Hinblick auf ihre gegebenen Wahrnehmungs-,
Kommunikations- und Entscheidungsfähigkeiten nicht besonders gut angepasst an
die Anforderungen des Straßenverkehrs. Die Wahrnehmung der Umgebung findet
überwiegend über den Sehsinn statt, doch die Augen erfassen zu einem Zeitpunkt
nur einen winzigen Teil der Szene. Die Sicht aus dem Fahrzeuginnenraum ist
zusätzlich  durch die Karosserie beeinträchtigt. Um sich ein möglichst
umfassendes Bild von der Umgebung und dem Zustand des Fahrzeugs zu machen, muss
ein Fahrer seinen Fokus in kurzer Folge auf eine große Zahl von Reizen richten,
wobei die Unterscheidung wichtiger von unwichtigen Reizen fällt besonders jungen
Fahrern schwer.\footcite{Deery2013} Eine Kommunikation mit Führern benachbarter
Fahrzeuge (analog zur Vernetzung fahrerloser Fahrzeuge) ist in der Regel
unmöglich.

Schlussendlich sind menschliche Entscheidungen durch eine
Reihe von kognitiven Verzerrungen beeinflusst. Prominent ist die Überschätzung
der eigenen Fähigkeiten: 80\% der Fahrer erachten sich besser als der
Durchschnitt,\footcite{McCormick1986} was zu einer übermäßig optimistischen und
risikofreudigen Fahrweise führt.\footcite[Vgl.][]{Svenson1981}

Fahrerlosen Fahrzeugen ist es hingegen möglich, jederzeit wie konzentrierte
professionelle Fahrer zu fahren. Möglicherweise können sie diese sogar
übertreffen, wenn man die besseren Möglichkeiten der Erfassung der Umgebung in
Betracht zieht. Während Menschen durch zahlreiche Faktoren hinsichtlich ihrer
Reaktion, Aufmerksamkeit und Wahrnehmung beschränkt sind, sind autonome
Fahrzeuge mit einer ganzen Reihe von Sensoren und Systemen ausgestattet, die es
ihnen erlauben, sich so sicher wie möglich im Straßenverkehr zu bewegen.
LiDAR-Sensoren erlauben es, die Umgebung und andere Verkehrsteilnehmer lückenlos
zu erfassen, Radar-Sensoren und Kameras überwachen den Fern- und Nahbereich vor
dem Fahrzeug. Bei korrekter Funktion ist es nahezu unmöglich, dass
Verkehrsteilnehmer übersehen werden, wohingegen der \enquote{tote Winkel} bei
konventionellen Fahrzeugen als unumstößliches Faktum betrachtet wird. Die
Steuerung eines fahrerlosen Fahrzeugs lässt sich schließlich so programmieren,
dass sie jeden Unfall vermeidet, der physikalisch vermeidbar ist.

%%
% \section{[Synthese]}

\vspace{1cm}

Die Forderung, von Menschen gelenkte Fahrzeuge durch fahrerlose abzulösen, ist
nur begründet, wenn ihre Umsetzung eine Besserung erwarten lässt.

Wollte man sie nach dem Maximin-Kriterium bewerten, müsste man danach fragen,
wie groß der schlimmste Schaden ist, der jeweils eintreffen kann. Allerdings
erweist sich dieses Kriterium als schwer anwendbar, da viele Auswirkungen
momentan gar nicht abzusehen sind.

Daher ist eine Bewertung anhand des Vorsorgeprinzips angezeigt.
Die potentielle Verminderung der zahlreichen Kosten, die durch Verkehr
entstehen, durch fahrerlose Fahrzeuge dient der Ressourcenvorsorge, also dem
Zweck, Umweltressourcen für eine zukünftige Nutzung zu
erhalten.\footcite[Vgl.][S.
391]{calliess2013} Allerdings bergen die Einführung fahrerloser Fahrzeuge und
schließlich die komplette Umstellung des öffentlichen Straßenverkehrs auch
Risiken, die zum jetzigen Zeitpunkt nicht vollständig benannt und bewertet
werden können. Es sind daher Maßnahmen zur Risikovorsorge nötig, von denen
einige schon eingangs beschrieben wurden.
Die Einführung kann nur schrittweise in anfangs kleinen, zunehmend wachsenden
Einsatzbereichen (Teststrecken, eingegrenzte Gelände, einzelne Städte usw.)
erfolgen. Die dabei gewonnenen Erkenntnisse müssen dazu dienen, gesetzliche
Vorgaben und öffentliche Institutionen zur Kontrolle zu schaffen, neue Techniken zu
entwickeln und die nächsten Schritte zu planen.

Die vorausgehenden Betrachtungen münden in die Prognose, dass fahrerlose
Fahrzeuge, die für den Einsatz im Straßenverkehr optimiert sind, dem Menschen im Hinblick
auf Wahrnehmungs-, Kommunikations- und Entscheidungsfähigkeiten überlegen sein
werden. Steigt ihr Anteil im Straßenverkehr, wird die Gefährdung von Menschen
durch Fehlleistungen menschlicher Fahrer überproportional sinken.

Als Fahrgast eines autonomen Fahrzeugs könnte sich der Vater aus der
einleitenden Kurzgeschichte den Tag am Meer ausmalen, die sportliche Fahrt
genießen und sich ganz auf seine Familie konzentrieren, während das Kind
ungefährdet vom Fluss träumen könnte. Dass sich ihre Wege kreuzen, hätte von
beiden unbemerkt bleiben können.

% -- Literaturverzeichnis --------
\newpage
\printbibliography

%%%
% Eigenständigkeitserklärung
\clearpage
\pagestyle{empty}

\centering
Christian-Albrechts-Universität zu Kiel\\
\textbf{Philosophisches Seminar}

\textbf{Erklärung}

\raggedright

\textbf{Name, Vorname: } Schröder, Simon-Martin\\
\textbf{Matrikel-Nummer: } 5279

 Hiermit versichere ich, dass ich den Essay mit dem Titel
 \enquote{\thetitle} selbständig verfasst und keine anderen als die angegebenen
 Quellen und Hilfsmittel benutzt habe. Ich habe alle Passagen, die ich aus
 gedruckten Schriften oder digital verfügbaren Dokumenten übernommenen habe,
 gekennzeichnet und korrekt zitiert. Ferner versichere ich, dass ich die
 vorliegende Arbeit in keinem anderen Prüfungsverfahren eingereicht habe.
 
 \line(1,0){250}\\
 Ort, Datum, Unterschrift
 
 Bei Täuschungsversuchen findet §21 der Prüfungsverfahrensordnung für Studierende der Bachelor- und Master-Studiengänge Anwendung.

\end{document}
